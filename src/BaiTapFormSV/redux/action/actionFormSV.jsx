import { THEM_SINH_VIEN } from "./../constant/constFormSV";

export const themSinhVien = (sinhVien) => {
  return {
    type: THEM_SINH_VIEN,
    payload: sinhVien,
  };
};
