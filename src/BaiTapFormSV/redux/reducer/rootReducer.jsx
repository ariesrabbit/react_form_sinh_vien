import { combineReducers } from "redux";
import { formSVReducer } from "./formSVReducer";

export const rootReducer_formSV = combineReducers({ formSVReducer });
