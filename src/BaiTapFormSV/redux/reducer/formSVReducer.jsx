import { THEM_SINH_VIEN } from "./../constant/constFormSV";
const initialState = {
  mangSinhVien: [
    {
      maSV: "1",
      hoTen: "Thỏ",
      soDienThoai: "0909090909",
      email: "ariesrabbit@gmail.com",
    },
  ],
};

export const formSVReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case THEM_SINH_VIEN: {
      let clonemangSinhVien = [...state.mangSinhVien, payload];
      state.mangSinhVien = clonemangSinhVien;
      return { ...state };
    }

    default:
      return { ...state };
  }
};
