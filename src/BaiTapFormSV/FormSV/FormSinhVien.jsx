import React, { Component } from "react";
import { connect } from "react-redux";
import { themSinhVien } from "../redux/action/actionFormSV";
import { THEM_SINH_VIEN } from "../redux/constant/constFormSV";

class FormSinhVien extends Component {
  state = {
    values: {
      maSV: "",
      hoTen: "",
      soDienThoai: "",
      email: "",
    },
    errors: {
      maSV: "",
      hoTen: "",
      soDienThoai: "",
      email: "",
    },
  };
  handleOnchange = (event) => {
    // lấy giá trị mỗi lần ô input bị thay đổi bởi người dùng
    let tagInput = event.target;
    let { name, value, type, pattern } = tagInput;

    let errorMessage = "";

    // kiểm tra rỗng
    if (value.trim() === "") {
      errorMessage = "Không được để trống !";
    }
    // kiểm tra email
    if (type === "email") {
      const regex = new RegExp(pattern);
      if (!regex.test(value)) {
        errorMessage = name + " không đúng định dạng";
      }
    }
    // kiểm tra số
    if (name === "soDienThoai") {
      const regex = new RegExp(pattern);
      if (!regex.test(value)) {
        errorMessage = name + " không đúng định dạng";
      }
    }

    // cập nhật giá trị value cho state
    let values = { ...this.state.values, [name]: value };
    // cập nhật giá trị error cho state
    let errors = { ...this.state.errors, [name]: errorMessage };
    this.setState({
      values: values,
      errors: errors,
    });
  };
  handleSubmit = (event) => {
    // gửi giá trị mới lên store của redux
    event.preventDefault();
    this.props.handleThemSinhVien(this.state.values);
  };
  renderButtonSubmit = () => {
    let valid = true;
    for (let key in this.state.errors) {
      if (this.state.errors[key] !== "") {
        valid = false;
      }
    }
    if (valid) {
      return (
        <button type="submit" className="btn btn-success">
          Thêm sinh viên
        </button>
      );
    } else
      return (
        <button type="submit" disabled={true} className="btn btn-success">
          Thêm sinh viên
        </button>
      );
  };
  render() {
    return (
      <div className="container">
        <div className="card text-left">
          <div className="card-header bg-dark text-white">
            <h3>Thông tin sinh viên</h3>
          </div>
          <div className="card-body">
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="form-group col-6">
                  <span>Mã SV</span>
                  <input
                    className="form-control"
                    type="text"
                    name="maSV"
                    value={this.state.values.maSV}
                    onChange={this.handleOnchange}
                  />
                  <p className="text-danger">{this.state.errors.maSV}</p>
                </div>
                <div className="form-group col-6">
                  <span>Họ tên</span>
                  <input
                    className="form-control"
                    type="text"
                    name="hoTen"
                    value={this.state.values.hoTen}
                    onChange={this.handleOnchange}
                  />
                  <p className="text-danger">{this.state.errors.hoTen}</p>
                </div>
              </div>
              <div className="row">
                <div className="form-group col-6">
                  <span>Số điện thoại</span>
                  <input
                    className="form-control"
                    type="text"
                    name="soDienThoai"
                    pattern="^(0|[1-9][0-9]*)$"
                    value={this.state.values.soDienThoai}
                    onChange={this.handleOnchange}
                  />
                  <p className="text-danger">{this.state.errors.soDienThoai}</p>
                </div>
                <div className="form-group col-6">
                  <span>Email</span>
                  <input
                    className="form-control"
                    type="email"
                    name="email"
                    pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
                    value={this.state.values.email}
                    onChange={this.handleOnchange}
                  />
                  <p className="text-danger">{this.state.errors.email}</p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 text-right">
                  {this.renderButtonSubmit()}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    handleThemSinhVien: (sinhVien) => {
      dispatch(themSinhVien(sinhVien));
    },
  };
};

export default connect(null, mapDispatchToProps)(FormSinhVien);
