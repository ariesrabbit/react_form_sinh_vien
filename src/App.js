import "./App.css";
import BaiTapForm from "./BaiTapFormSV/FormSV/BaiTapForm";

function App() {
  return (
    <div className="App">
      <BaiTapForm />
    </div>
  );
}

export default App;
